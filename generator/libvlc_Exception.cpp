/*****************************************************************************
 * libvlc_Exception.cpp: Exception implementation
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <vlc++/libvlc_common.hpp>

namespace VLC {

Exception::Exception(const std::string& defaultMessage) {
    const char* err = libvlc_errmsg();
    if (err) {
        message = err;
    } else {
        message = defaultMessage;
    }
    message = "LibVLC error: " + message;
    libvlc_clearerr();
}

const char* Exception::what() const throw() {
    return message.c_str();
}

Exception::~Exception() throw() {}

void Exception::checkError() {
    if (libvlc_errmsg()) {
        throw Exception();
    }
}

} // namespace VLC

