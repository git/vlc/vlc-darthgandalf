/*****************************************************************************
 * vlc_perl.i: Perl specifics of LibVLC++ bindings.
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

%include "vlc_perl_renames.i"

%typemap(in) (int argc, char const*const*argv) {
    AV *tempav;
    I32 len;
    int i;
    SV  **tv;
    if (!SvROK($input))
        croak("Argument $argnum is not a reference.");
    if (SvTYPE(SvRV($input)) != SVt_PVAV)
        croak("Argument $argnum doesn't refer to an array.");
    tempav = (AV*)SvRV($input);
    len = av_len(tempav);
    $2 = (char **) malloc((len+2)*sizeof(char *));
    for (i = 0; i <= len; i++) {
        tv = av_fetch(tempav, i, 0);
        $2[i] = (char *) SvPV_nolen(*tv);
    }
    $2[i] = NULL;
    $1 = len + 1;
}

%typemap(freearg) (int argc, char const*const*argv) {
    if ($2) free($2);
}

%typecheck(0) (int argc, char const*const*argv) {
    if (!SvROK($input)) {
        $1 = 0;
    } else if (SvTYPE(SvRV($input)) != SVt_PVAV) {
        $1 = 0;
    } else {
        $1 = 1;
    }
}

%typemap(out) std::list<std::string> { /* for _get_modules_list */
    AV *myav;
    int i = 0;
    int len = $1.size();
    SV** svs = (SV **) malloc(len*sizeof(SV *));
    for (std::list<std::string>::const_iterator it = $1.begin(); it != $1.end(); ++it) {
        svs[i] = sv_newmortal();
        sv_setpv((SV*)svs[i], it->c_str());
        i++;
    };
    myav = av_make(len,svs);
    free(svs);
    $result = newRV_noinc((SV*)myav);
    sv_2mortal($result);
    argvi++;
}

%perlcode %{
package VideoLAN::LibVLC;
our $VERSION = "0.0_1"; # FIXME

=head1 WARNING!

This module is experimental, and API will change in future versions. Use with care, or do not use at all until experimental phase is over.

=cut

package VideoLAN::LibVLC::Instance;

*_c_audio_output_list = *audio_output_list;
sub _my_audio_output_list {
    my $self = shift;
    my $result = $self->_c_audio_output_list(@_);
    @$result;
}
*audio_output_list = *_my_audio_output_list;

*_c_video_filter_list = *video_filter_list;
sub _my_video_filter_list {
    my $self = shift;
    my $result = $self->_c_video_filter_list(@_);
    @$result;
}
*video_filter_list = *_my_video_filter_list;

*_c_audio_filter_list = *audio_filter_list;
sub _my_audio_filter_list {
    my $self = shift;
    my $result = $self->_c_audio_filter_list(@_);
    @$result;
}
*audio_filter_list = *_my_audio_filter_list;

package VideoLAN::LibVLC::Audio;

*_c_track_description = *track_description;
sub _my_track_description {
    my $self = shift;
    my $result = $self->_c_track_description(@_);
    @$result;
}
*track_description = *_my_track_description;

package VideoLAN::LibVLC::Media;

*_c_tracks_info = *tracks_info;
sub _my_tracks_info {
    my $self = shift;
    my $result = $self->_c_tracks_info;
    @$result;
}
*tracks_info = *_my_tracks_info;

*_c_new = *new;
sub _my_new {
    my $class = shift;
    if ($#_ == 2 && ref $_[0] && $_[0]->isa("VideoLAN::LibVLC::Instance")) {
        my ($inst, $type, $value) = @_;
        my $mtype;
        if ($type eq "location") {
            $mtype = $VideoLAN::LibVLC::Media::Location;
        } elsif ($type eq "path") {
            $mtype = $VideoLAN::LibVLC::Media::Path;
        } elsif ($type eq "node") {
            $mtype = $VideoLAN::LibVLC::Media::Node;
        }
        if (defined $mtype) {
            return $class->_c_new($inst, $value, $mtype);
        }
    }
    $class->_c_new(@_)
}
*new = *_my_new;

%}

