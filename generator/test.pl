#!/usr/bin/env perl

use strict;
use warnings;

use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;
my $media = VideoLAN::LibVLC::Media->new($inst, $ARGV[0]);
my $player = VideoLAN::LibVLC::MediaPlayer->new($media);

$player->play;
sleep 10;
