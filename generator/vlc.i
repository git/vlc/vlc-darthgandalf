/*****************************************************************************
 * vlc.i: SWIG interface to LibVLC++.
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef SWIGPERL
%include <noembed.h>
%{
#ifdef seed
# undef seed
#endif
%}
#endif

%include <typemaps.i>
%include <stl.i>
%include <std_string.i>
%include <std_list.i>
%include <std_vector.i>
%include <exception.i>
%include <stdint.i>

%exception {
    try {
        $action
    } catch (const std::exception& e) {
        SWIG_exception(SWIG_RuntimeError, e.what());
    }
}

%ignore libvlc_vprinterr;
%ignore libvlc_printerr;

%template(list_TrackDescription) std::list<VLC::TrackDescription>;
%template(list_ModuleDescription) std::list<VLC::ModuleDescription>;
%template(list_AudioOutputDescription) std::list<VLC::AudioOutputDescription>;
%template(vector_MediaTrackInfo) std::vector<VLC::MediaTrackInfo>;

%apply int *OUTPUT {int *px, int *py};
%apply unsigned *OUTPUT {unsigned *px, unsigned *py};

/*
   There's no good portable way to produce char** from any non-C language.
   These typemaps are to be able to call libvlc_new(argc, argv)
   Here in both cases reference to a list of strings is converted to argc+argv.
Python: inst = VLC.libvlc_new(["vlc", "foo", "bar"])
Perl: my $inst = VLC::libvlc_new(["vlc", "foo", "bar"])
*/

#ifdef SWIGPYTHON
%include "vlc_python.i"
#endif

#ifdef SWIGPERL
%include "vlc_perl.i"
#endif

#ifdef SWIGRUBY
%include "vlc_ruby.i"
#endif

%begin %{
#include <vlc++/vlc.hpp>
%}
%include <vlc/libvlc_structures.h>
%include <vlc/libvlc.h>
%include <vlc/libvlc_media.h>
%include <vlc/libvlc_media_player.h>
%include <vlc/libvlc_media_list.h>
%include <vlc/libvlc_media_list_player.h>
%include <vlc/libvlc_media_library.h>
%include <vlc/libvlc_media_discoverer.h>
%include <vlc/libvlc_events.h>
%include <vlc/libvlc_vlm.h>

%include <vlc_all_cxx_headers.i>

%inline %{
    std::list<std::string> _get_modules_list() {
        std::list<std::string> result;
        int old_out = dup(1);
        if (old_out == -1) {
            throw std::exception();
        }
        int fd[2];
        if (pipe(fd)) {
            close(old_out);
            throw std::exception();
        }
        close(1);
        if (1 != dup(fd[1])) {
            abort();
        }
        close(fd[1]);
        const char* const argv[] = {"vlc", "-l"};
        libvlc_new(2, argv);
        fflush(stdout);
        close(1); // this was fd[1]
        if (1 != dup(old_out)) {
            abort();
        }
        close(old_out);

        FILE* f = fdopen(fd[0], "rt");
        char buf[1024];
        while (fgets(buf, 1023, f)) {
            // I hope, no line will be longer than 1022 characters...
            // If not, it won't crash, but just give weird output
            char name[1024];
            sscanf(buf, "%s", name);
            result.push_back(name);
        }
        fclose(f); // closes fd[0]

        return result;
    }
%}

