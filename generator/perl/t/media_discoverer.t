#!perl -T

use Test::More tests => 2;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;

my $modules = VideoLAN::LibVLC::_get_modules_list();

SKIP: {
	skip "vlc without mediadirs", 2 unless grep /^mediadirs$/, @$modules;
    my $disc = new_ok('VideoLAN::LibVLC::MediaDiscoverer' => [$inst, 'mediadirs']);
    can_ok($disc, 'localized_name', 'is_running')
}

