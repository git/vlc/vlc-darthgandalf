#!perl -T

use Test::More tests => 5;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;

my $player = new_ok('VideoLAN::LibVLC::MediaPlayer' => [$inst]);

eval { $player->media };
ok($@, 'No media in player');

my $media = VideoLAN::LibVLC::Media->new($inst, location => 'vlc://nop');
$player = new_ok('VideoLAN::LibVLC::MediaPlayer' => [$media]);
cmp_ok($player->media, '==', $media, "It's the same media");

ok(!$player->is_playing, 'Not playing');
