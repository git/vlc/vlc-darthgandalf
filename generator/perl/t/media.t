#!perl -T

use Test::More tests => 14;
use VideoLAN::LibVLC;
use File::Spec;

my $inst = VideoLAN::LibVLC::Instance->new;

my $media_location = new_ok('VideoLAN::LibVLC::Media' => [$inst, location => 'file:///foo/bar']);
is($media_location->mrl, 'file:///foo/bar', 'Media by location');

my $media_node = new_ok('VideoLAN::LibVLC::Media' => [$inst, node => 'foo']);
is($media_node->mrl, 'vlc://nop', 'Media by node');

SKIP: {
	skip 'Not UNIX', 2 unless File::Spec->catdir('foo', 'bar') eq 'foo/bar';
	my $media_path = new_ok('VideoLAN::LibVLC::Media' => [$inst, path => '/foo/bar']);
	is($media_path->mrl, 'file:///foo/bar', 'Media by UNIX path');
}

SKIP: {
	skip 'Not Windows', 2 unless File::Spec->catdir('foo', 'bar') eq 'foo\\bar';
	my $media_path = new_ok('VideoLAN::LibVLC::Media' => [$inst, path => 'C:\\foo\\bar']);
	is($media_path->mrl, 'file:///C:/foo/bar', 'Media by Windows path');
}

my $media_duplicate = $media_location->duplicate;
isa_ok($media_duplicate, 'VideoLAN::LibVLC::Media', 'Duplicated Media');
is($media_duplicate->mrl, $media_location->mrl, 'Duplicated Media MRL');

my $media_constr = new_ok('VideoLAN::LibVLC::Media' => [$inst, 'file:///foo/bar']);
is($media_constr->mrl, 'file:///foo/bar', 'Constructor with default arg');

my $media = VideoLAN::LibVLC::Media->new($inst, 'vlc://nop');
cmp_ok($media->is_parsed, '==', 0, 'Media not parsed yet');
$media->parse;
cmp_ok($media->is_parsed, '==', 1, 'Media is already parsed');

# tracks_info
# meta
# stats

my $modules = VideoLAN::LibVLC::_get_modules_list();

SKIP: {
	skip "vlc without ffmpeg support", 0 unless grep /^avcodec$/, @$modules and grep /^avformat$/, @$modules;
	my $media = VideoLAN::LibVLC::Media->new($inst, path => 't/test.mpg');
	$media->parse;
	my $player = VideoLAN::LibVLC::MediaPlayer->new($media);
	$player->play;
	sleep 1;
	diag $media->tracks_info;
}

