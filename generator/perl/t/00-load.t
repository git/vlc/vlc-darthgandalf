#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'VideoLAN::LibVLC' ) || print "Bail out!
";
}

diag( "Testing VideoLAN::LibVLC $VideoLAN::LibVLC::VERSION, Perl $], $^X" );
