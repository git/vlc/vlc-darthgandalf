#!perl -T

use Test::More tests => 4;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;
my $media = VideoLAN::LibVLC::Media->new($inst, path => 't/test.mpg');
my $player = VideoLAN::LibVLC::MediaPlayer->new($media);
$player->play;
my $audio = $player->audio;

isa_ok($audio, 'VideoLAN::LibVLC::Audio');
ok(!$audio->mute, 'Not muted');
$audio->toggle_mute;
ok($audio->mute, 'Muted');
$audio->mute(0);
ok(!$audio->mute, 'Not muted again');

diag $audio->track_count
