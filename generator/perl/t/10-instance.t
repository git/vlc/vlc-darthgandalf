#!perl -T

use Test::More tests => 5;
use VideoLAN::LibVLC;

my $inst = new_ok('VideoLAN::LibVLC::Instance');
can_ok($inst, qw(add_intf audio_filter_list audio_output_list user_agent));

my @outputs = $inst->audio_output_list;
diag("Audio outputs count: ".scalar @outputs);

SKIP: {
	skip 'No audio outputs found', 1 unless @outputs;
	isa_ok($outputs[0], 'VideoLAN::LibVLC::AudioOutputDescription');
}

my @filters = $inst->audio_filter_list;
diag("Audio filters count: ".scalar @filters);

SKIP: {
	skip 'No audio filters found', 1 unless @filters;
	isa_ok($filters[0], 'VideoLAN::LibVLC::ModuleDescription');
}

@filters = $inst->video_filter_list;
diag("Video filters count: ".scalar @filters);

SKIP: {
	skip 'No video filters found', 1 unless @filters;
	isa_ok($filters[0], 'VideoLAN::LibVLC::ModuleDescription');
}

