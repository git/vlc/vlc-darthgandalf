#!perl -T

use Test::More tests => 8;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;

my $list = new_ok('VideoLAN::LibVLC::MediaList' => [$inst]);
cmp_ok($list->count, '==', 0, 'Empty list is empty');

my $media = VideoLAN::LibVLC::Media->new($inst, location => 'vlc://nop');
$list->add_media($media);
cmp_ok($list->count, '==', 1, 'Not empty');
isa_ok($list->item_at_index(0), 'VideoLAN::LibVLC::Media');
eval { $list->item_at_index(1) };
ok($@, 'Index out of bounds');
cmp_ok($list->index_of_item($media), '==', 0, 'Search in list');
eval { $list->index_of_item($media->duplicate) };
ok($@, 'Not in list');
$list->remove_index(0);
cmp_ok($list->count, '==', 0, 'Empty again');
