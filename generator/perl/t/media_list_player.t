#!perl -T

use Test::More tests => 2;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;

my $player = new_ok('VideoLAN::LibVLC::MediaListPlayer' => [$inst]);

ok(!$player->is_playing, 'Not playing');
