#!perl -T

#use Test::More tests => 10;
use Test::More skip_all => 'Logging API is broken by design in current VLC';
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;
cmp_ok($inst->log_verbosity, '==', 1, 'Initial log verbosity');
$inst->log_verbosity(3);
cmp_ok($inst->log_verbosity, '==', 3, 'Set log verbosity');

my $log = new_ok('VideoLAN::LibVLC::Log' => [$inst]);

my $empty_iter = new_ok('VideoLAN::LibVLC::LogIterator' => [$log]);
TODO: {
	local $TODO = "Logs API is kinda broken IMHO";
	cmp_ok($empty_iter->has_next, '==', 0, 'No messages in empty iter');

	eval { $empty_iter->next };
	ok($@, 'Empty iter dies on next');
}

my $media = VideoLAN::LibVLC::Media->new($inst, location=>'file:///foo/bar');
$media->parse;

my $iter = new_ok('VideoLAN::LibVLC::LogIterator' => [$log]);
my $count = 0;

SKIP: {
	skip "No messages in log", 1 unless $iter->has_next;
	my $tested = 0;
	while ($iter->has_next) {
		my $msg = $iter->next;
		unless ($tested) {
			$tested = 1;
			isa_ok($msg, 'VideoLAN::LibVLC::LogMessage', 'Message is message');
		}
		my $severity = $msg->severity // '';
		my $type = $msg->type // '';
		my $name = $msg->name // '';
		my $header = $msg->header // '';
		my $message = $msg->message // '';
		note "s=$severity, t=$type, n=$name, h=$header, m=$message";
		$count++;
	}
}

TODO: {
	local $TODO = "Logs API is kinda broken IMHO";
	cmp_ok($count, '==', $log->count, 'Got every message and nothing else');
	cmp_ok($empty_iter->has_next, '==', 0, 'Empty iter is still empty');
}
