#!perl -T

use Test::More tests => 2;
use VideoLAN::LibVLC;

my $inst = VideoLAN::LibVLC::Instance->new;

my $lib = new_ok('VideoLAN::LibVLC::MediaLibrary' => [$inst]);
can_ok($lib, 'load');
