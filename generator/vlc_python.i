/*****************************************************************************
 * vlc_python.i: Python specifics of LibVLC++ bindings.
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

%typemap(in) (int argc, char const*const*argv) %{
    {
        int i;
        PyObject* fast = PySequence_Fast($input, "Expecting a sequence");
        if (!fast) {
            return NULL;
        }
        $1 = PySequence_Size($input);
        $2 = (char **) malloc(($1+1)*sizeof(char *));

        for (i = 0; i < $1; i++) {
            PyObject *s = PySequence_Fast_GET_ITEM($input, i);
#if PY_VERSION_HEX >= 0x03000000
            if (!PyUnicode_Check(s)) {
                free($2);
                Py_CLEAR(fast);
                PyErr_SetString(PyExc_TypeError, "Sequence items must be strings");
                return NULL;
            }
            PyObject* bytes = PyUnicode_AsUTF8String(s);
            if (!bytes) {
                free($2);
                Py_CLEAR(fast);
                return NULL;
            }
            $2[i] = strdup(PyBytes_AsString(bytes));
            Py_CLEAR(bytes);
#else
            if (!PyString_Check(s)) {
                free($2);
                Py_CLEAR(fast);
                PyErr_SetString(PyExc_TypeError, "Sequence items must be strings");
                return NULL;
            }
            $2[i] = strdup(PyString_AsString(s));
#endif
        }
        $2[i] = NULL;
        Py_CLEAR(fast);
    }
%}

%typemap(freearg) (int argc, char const*const*argv) {
    if ($2) {
        int i;
        for (i = 0; i < $1; i++) {
            free($2[i]);
        }
        free($2);
    }
}

%typecheck(SWIG_TYPECHECK_POINTER) (int argc, char const*const*argv) {
    $1 = PySequence_Check($input);
}

