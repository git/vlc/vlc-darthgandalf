#############################################################################
# docgen.py: Generate documentation based on doxygen xml.
#############################################################################
# Copyright © 2011 the VideoLAN team
#
# Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#############################################################################
'''Generate documentation for generated classes in different formats.

Generated documentation is based on Doxygen's XML output.'''

import textwrap
import model
import re

# TODO Code duplication is bad. Need to move common stuff to a separate class.

class DocumentationGenerator:
    '''Base class for documentation generators.

    Children are expected to define put_header, put_footer, put_line, put_seppara, put_node.
    '''
    def __init__(self, file, namer):
        self.file = file
        self.namer = namer

    def put_docs(self, method, skip=0, remove_return=False):
        '''Generate the docs for the given method.

        Skip few arguments from beginning, probably omit return statemement.'''
        self.method = method
        try:
            self.name = self.namer.make_name(method)
        except AttributeError:
            pass
        node = method.func.node
        self.skipped = 0
        self.skip = skip
        self.remove_return = remove_return
        self.put_header()
        self.first_para = True
        for para in node.xpath('detaileddescription/para'):
            self.current_line = ''
            self.put_node(para)
            self.finish_para()
        self.put_footer()

    def add_text(self, text):
        '''Write more text to current paragraph.'''
        self.current_line += text.strip() + ' '

    def finish_para(self):
        '''Finish current paragraph.'''
        stripped = self.current_line.strip()
        if stripped == '':
            return
        if self.first_para:
            self.first_para = False
        else:
            self.put_seppara()
        for s in textwrap.wrap(stripped):
            self.put_line(s.strip())
        self.current_line = ''

class Doxygen(DocumentationGenerator):
    '''Generate doxygen docs for C++ headers.'''
    def put_header(self):
        self.file.put('    /**')

    def put_footer(self):
        self.file.put('     */')

    def put_line(self, line=''):
        self.file.put('     * ' + line)

    def put_seppara(self):
        self.file.put('     *')

    def put_node(self, node):
        if node.text is not None:
            self.add_text(node.text)
        for x in node:
            if x.tag == 'para':
                self.put_node(x)
            elif x.tag == 'simplesect':
                kind = x.attrib['kind']
                self.finish_para()
                if self.remove_return and kind in set(('return', 'returns')):
                    pass
                elif kind == 'par' and x.xpath('title = "LIBVLC_RETURN_BOOL"'):
                    pass
                else:
                    self.add_text('\\{}'.format(kind))
                    self.put_node(x)
            elif x.tag == 'simplesectsep':
                self.add_text('')
            elif x.tag == 'ref':
                name = x.text.rstrip('()')
                if name in model.functions:
                    f = model.functions[name]
                    if type(f) == model.Constructor:
                        self.add_text('{t}::{t}()'.format(t=f.cls.cls_name))
                    else:
                        self.add_text('{}::{}()'.format(f.cls.cls_name, self.namer.make_name(f)))
                elif name in model.type2class:
                    cls = model.type2class[name]
                    self.add_text(cls.cls_name)
                else:
                    self.add_text(x.text)
            elif x.tag == 'parameterlist':
                kind = x.attrib['kind']
                for item in x:
                    if self.skip > self.skipped:
                        self.skipped += 1
                        continue
                    name = item.xpath('parameternamelist/parametername')[0].text.strip()
                    desc = item.xpath('parameterdescription')[0]
                    self.finish_para()
                    self.add_text('\\{kind} {name}'.format(kind=kind, name=name))
                    self.put_node(desc)
            if x.tail is not None and x.tail != '':
                self.add_text(x.tail)

class POD(DocumentationGenerator):
    '''Generate documentation for Perl.'''
    def put_header(self):
        self.file.put('\n\n=head3 {}\n'.format(self.name))

    def put_footer(self):
        self.file.put('\n=cut\n')

    def put_line(self, line=''):
        self.file.put(line)

    def put_seppara(self):
        self.file.put('')

    def put_node(self, node):
        if node.text is not None:
            self.add_text(node.text)
        for x in node:
            if x.tag == 'para':
                self.put_node(x)
            elif x.tag == 'simplesect':
                kind = x.attrib['kind']
                self.finish_para()
                if self.remove_return and kind in set(('return', 'returns')):
                    pass
                elif kind == 'par' and x.xpath('title = "LIBVLC_RETURN_BOOL"'):
                    pass
                else:
                    self.add_text('B<{}>'.format(kind))
                    self.put_node(x)
            elif x.tag == 'simplesectsep':
                self.add_text(',')
            elif x.tag == 'ref':
                name = x.text.rstrip('()')
                if name in model.functions:
                    f = model.functions[name]
                    if type(f) == model.Constructor:
                        self.add_text('L<< VideoLAN::LibVLC::{}/new >>'.format(f.cls.cls_name))
                    else:
                        self.add_text('L<< VideoLAN::LibVLC::{}/{} >>'.format(f.cls.cls_name, self.namer.make_name(f)))
                elif name in model.type2class:
                    cls = model.type2class[name]
                    self.add_text('L<< VideoLAN::LibVLC::{} >>'.format(cls.cls_name))
                else:
                    self.add_text(x.text)
            elif x.tag == 'parameterlist':
                kind = x.attrib['kind']
                have_over = False
                for item in x:
                    if self.skip > self.skipped:
                        self.skipped += 1
                        continue
                    name = item.xpath('parameternamelist/parametername')[0].text.strip()
                    desc = item.xpath('parameterdescription')[0]
                    self.finish_para()
                    if not have_over:
                        self.put_line('\n=over\n')
                        have_over = True
                    self.put_line('\n=item B<< {kind} >> {name}\n'.format(kind=kind, name=name, desc=desc))
                    self.put_node(desc)
                if have_over:
                    self.finish_para()
                    self.put_line('\n=back\n')
            if x.tail is not None and x.tail != '':
                self.add_text(x.tail)

class RDoc(DocumentationGenerator):
    '''Generate documentation for Ruby.'''
    def put_header(self):
        self.indent = 0
        pass

    def put_footer(self):
        pass

    r = re.compile(r'"')
    def put_line(self, line=''):
        self.file.put(' '*self.indent + self.r.sub(r'\"', line))

    def put_seppara(self):
        self.file.put('')

    def put_node(self, node):
        if node.text is not None:
            self.add_text(node.text)
        for x in node:
            if x.tag == 'para':
                self.put_node(x)
            elif x.tag == 'simplesect':
                kind = x.attrib['kind']
                self.finish_para()
                if self.remove_return and kind in set(('return', 'returns')):
                    pass
                elif kind == 'par' and x.xpath('title = "LIBVLC_RETURN_BOOL"'):
                    pass
                else:
                    self.put_line('==== {}'.format(kind))
                    self.put_node(x)
            elif x.tag == 'simplesectsep':
                self.add_text(',')
            elif x.tag == 'ref':
                name = x.text.rstrip('()')
                if name in model.functions:
                    f = model.functions[name]
                    if type(f) == model.Constructor:
                        self.add_text('LibVLC::{}.new'.format(f.cls.cls_name))
                    else:
                        self.add_text('LibVLC::{}#{}'.format(f.cls.cls_name, self.namer.make_name(f)))
                elif name in model.type2class:
                    cls = model.type2class[name]
                    self.add_text('LibVLC::{}'.format(cls.cls_name))
                else:
                    self.add_text(x.text)
            elif x.tag == 'parameterlist':
                kind = x.attrib['kind']
                have_over = False
                for item in x:
                    if self.skip > self.skipped:
                        self.skipped += 1
                        continue
                    name = item.xpath('parameternamelist/parametername')[0].text.strip()
                    desc = item.xpath('parameterdescription')[0]
                    self.finish_para()
                    if not have_over:
                        self.put_line('==== Parameters')
                        have_over = True
                    self.put_line('<em>{name}</em>::'.format(kind=kind, name=name, desc=desc))
                    self.indent += 4
                    self.put_node(desc)
                    self.finish_para()
                    self.indent -= 4
                if have_over:
                    self.finish_para()
            if x.tail is not None and x.tail != '':
                self.add_text(x.tail)
