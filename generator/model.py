#############################################################################
# model.py: Structure of generated classes and their methods
#############################################################################
# Copyright © 2011 the VideoLAN team
#
# Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#############################################################################
'''Define concepts of classes and their methods.

Class is chosen based on first parameter type (for methods), or
from return value type (for constructors).

Also there're subclasses: several classes can have the same first parameter type.
In this case the subclass is chosen based on function name.
It's not really subclass in usual definition of a subclass, but this word fits
our needs pretty much.
'''

import re

from lxml import etree
from copy import deepcopy

type2class = dict()
classes = dict()
functions = dict()
unwrapped = list()

class Type:
    '''Type of a function parameter, of a function return value, etc.'''
    def __init__(self, node):
        self.node = node
        self.changed = node.attrib.get('changed') == '1'
        self.c_name = node.attrib.get('c_name')
        self.cls_name = node.attrib.get('cls_name')

    api_re = re.compile(r'^LIBVLC_(?:API|DEPRECATED)\s+')
    def real_str(self):
        '''Serialize the type into string describing C/C++ type.'''
        def getstr(node, use_tail=False):
            result = ''.join(getstr(n, True) for n in node);
            if node.text is not None:
                result = node.text + result
            if use_tail and node.tail is not None:
                result = result + node.tail
            return result
        s = getstr(self.node)
        return self.api_re.sub('', s)

    def classy_node(self):
        '''Convert C type into corresponding C++ type.

        Return XML node for C++ type.'''
        n = deepcopy(self.node)
        x = n.xpath('ref[.!="LIBVLC_API" and .!="LIBVLC_DEPRECATED"]')
        if len(x) > 1:
            raise Exception('More than 1 "ref" in a type')
        if len(x) > 0:
            r = x[0]
            if r.text in type2class:
                n.attrib['c_name'] = r.text
                r.text = type2class[r.text].cls_name
                n.attrib['changed'] = '1'
                n.attrib['cls_name'] = r.text
        return n

    def classy(self):
        '''Convert C type into corresponding C++ type.'''
        return Type(self.classy_node())

    def classy_ret_str(self):
        if self.changed:
            return self.cls_name
        return self.real_str()

    class VoidError(Exception):
        pass

class Param:
    '''Parameter of a function.'''
    def __init__(self, type, name):
        self.type = type
        self.name = name

    bool_params = (
            ('libvlc_set_fullscreen',        'b_fullscreen'),
            ('libvlc_video_set_key_input',   'on'),
            ('libvlc_video_set_mouse_input', 'on'),
            ('libvlc_audio_set_mute',        'status'),
            ('libvlc_vlm_add_broadcast',     'b_enabled'),
            ('libvlc_vlm_add_vod',           'b_enabled'),
            ('libvlc_vlm_change_media',      'b_enabled')
    )

    @classmethod
    def parse(cls, node, fname):
        type = Type(node.xpath('type')[0])
        typestr = type.real_str()
        if typestr == 'void':
            raise Type.VoidError()
        if typestr == '...':
            return Param(type, None)
        name = node.xpath('declname')[0].text
        if (fname, name) in Param.bool_params:
            type.node.text = 'bool'
        return Param(type, name)

    callback_re = re.compile(r'\(\*\)')
    pointer_re = re.compile(r'\*$')
    def __str__(self):
        t = self.type.real_str()
        if self.type.changed:
            t = self.pointer_re.sub('&', t)
        if self.callback_re.search(t):
            return self.callback_re.sub('(*{})'.format(self.name), t)
        return '{} {}'.format(t, self.name)

    def classy(self):
        return Param(self.type.classy(), self.name)

class Function:
    '''C function.'''
    depr_re = re.compile(r'^LIBVLC_DEPRECATED\s')
    def __init__(self, node):
        self.node = node
        self.name = node.xpath('name')[0].text
        self.params = []
        for p in node.xpath('param'):
            try:
                self.params.append(Param.parse(p, self.name))
            except Type.VoidError:
                pass
        self.deprecated = bool(Function.depr_re.match(etree.tostring(node.xpath('type')[0], method="text").decode()))
        self.type = Type(node.xpath('type')[0])
        self.overriden = False

class Method:
    '''Method of a class'''
    def __init__(self, func, words, cls):
        '''Make a method wrapping the function.

        First parameter of the function func is assumed to be C counterpart of the given class.'''
        self.func = func
        self.params = [p.classy() for p in func.params[1:]]
        self.type = func.type.classy()
        self.words = words
        self.cls = cls

class Constructor:
    '''Constructor of a class'''
    def __init__(self, func, cls):
        '''Make a constructor of the class wrapping the function.

        Return value of the function is used to produce internal C pointer.'''
        self.func = func
        self.params = [p.classy() for p in func.params]
        self.cls = cls

class Class:
    '''A class'''
    def __init__(self, c_name, cls_name, prefix='', copyable=True, releaser=None, need_wrap=True, result_maker='c_result', resultfreer='', result=None):
        self.c_name = c_name
        self.cls_name = cls_name
        self.methods = []
        self.private_methods = []
        self.constructors = []
        self.prefix = prefix
        self.prefix_re = re.compile('^' + prefix)
        self.copyable = copyable
        self.releaser = releaser
        self.subclasses = dict()
        self.need_wrap = need_wrap
        self.result_maker = result_maker
        self.resultfreer = resultfreer
        self.result = result

    def register(self):
        '''Add self to few global vars'''
        type2class[self.c_name] = self
        classes[self.cls_name] = self

    libvlc_prefix_re = re.compile('^libvlc_')
    def add_method(self, f):
        '''Wrap function f as a method and add it to the class.

        f's first parameter is assumed to be C counterpart of the class.'''
        class_under_name = self.libvlc_prefix_re.sub('', f.name)
        full_words = class_under_name.split('_')
        if full_words[0] in self.subclasses:
            return self.subclasses[full_words[0]].add_method(f)
        if full_words[0] == 'vlm':
            # VLM API is not very usable... It should be changed in libvlc itself.
            return

        under_name = self.prefix_re.sub('', class_under_name)
        words = under_name.split('_')
        m = Method(f, words, self)
        if f.overriden:
            if f.source.attrib.get('access', 'public') == 'private':
                self.private_methods.append(m)
            else:
                self.methods.append(m)
        else:
            if (len(words) == 1 and words[0] in set(('retain', 'release')) ) or f.name == self.releaser:
                self.private_methods.append(m)
            else:
                self.methods.append(m)
        return m

    def add_constructor(self, f):
        '''Use return value of f as a new constructor of the class.'''
        if f.name in set(('libvlc_media_new_path', 'libvlc_media_new_as_node')):
            # These functions produce conflicts when using without names (as a constructor).
            # Classy API has new constructor Media::Media(const std::string&, ConstructorType) instead.
            # Check override.xml (and generated libvlc_Media.hpp) for details.
            return
        c = Constructor(f, self)
        self.constructors.append(c)
        return c

    def add_subclass(self, name, prefix, **kwargs):
        cls = Class(c_name=self.c_name, cls_name=name, prefix=prefix+'_', **kwargs)
        self.subclasses[prefix] = cls
        classes[name] = cls

structs = dict()

class Var:
    '''A field of a struct'''
    pass

class Struct(Class):
    '''Struct.

    There're 2 kinds of structs in libvlc:
    - just structs,
    - structs with p_next pointers, which makes them to be a list.
    List-like structs are converted to real lists.'''
    r = re.compile(r'^[a-z]+_')
    @staticmethod
    def parse(xml, override, cname, clsname, resultfreer=None):
        node = xml.xpath('//compounddef[compoundname="{}"]'.format(cname))[0]
        params = []
        for varnode in node.xpath('sectiondef[@kind="public-attrib"]/memberdef[@kind="variable" and not(contains(type, "::@"))]'):
            v = Var()
            v.c_name = varnode.xpath('name')[0].text
            v.type = etree.tostring(varnode.xpath('type')[0], method="text").decode().strip()
            v.name = Struct.r.sub('', v.c_name)
            l = override.xpath('/override/struct[@name="{clsname}"]/field[@name="{v.c_name}"]'.format(**locals()))
            if l:
                v.c_name = l[0].text + v.c_name
            params.append(v)
        if 'p_next' in (x.c_name for x in params):
            if resultfreer is None:
                raise ValueError('{cname} looks like a list, resultfreer is needed'.format(**locals()))
            s = Struct(c_name=cname, cls_name=clsname, need_wrap=False,
                    resultfreer='\n    {}(c_result);'.format(resultfreer),
                    result_maker='{}::makeList(c_result)'.format(clsname),
                    result='std::list<{}>'.format(clsname))
            s.list = True
            for x in params:
                if x.c_name == 'p_next':
                    params.remove(x)
                    break
        else:
            s = Struct(c_name=cname, cls_name=clsname, need_wrap=False)
            s.list = False
        s.node = node
        s.params = params
        return s

    def register(self):
        structs[self.c_name] = self
        super().register()



def wrap_func(f):
    '''Wrap function f as a constructor or as a method for some class, what is more appropriate for it.

    Or add its name to unwrapped, if neither are.'''
    wrapped = False
    l = f.xpath('param[1]/type/ref')
    f = Function(f)
    if not f.deprecated:
        t = f.type.classy()
        if t.changed and classes[t.cls_name].need_wrap and f.name not in ('libvlc_media_duplicate', 'libvlc_media_list_item_at_index', 'libvlc_media_player_get_media') and t.cls_name != 'EventManager':
            x = classes[t.cls_name].add_constructor(f)
            if x is not None:
                functions[f.name] = x
            wrapped = True
        elif len(l) > 0:
            cls_cname = l[0].text
            if cls_cname in type2class:
                x = type2class[cls_cname].add_method(f)
                if x is not None:
                    functions[f.name] = x
            elif not wrapped:
                unwrapped.append('{} :: {}'.format(cls_cname, f.name))
        elif not wrapped:
            unwrapped.append(f.name)
    return f


