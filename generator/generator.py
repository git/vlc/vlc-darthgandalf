#############################################################################
# generator.py: Generate different sources based on model.
#############################################################################
# Copyright © 2011 the VideoLAN team
#
# Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#############################################################################
'''Generate different files based on the model of classes/methods of LibVLC.

Some documentation-related stuff isn't finished yet.'''

import os

import namer
import docgen
import model

class Generator:
    '''Base class for a generator.

    Generates stuff related to classes and their methods one by one.'''
    def __init__(self, datadir):
        '''Initialize new Generator, which will look for (license) headers in datadir.'''
        self.__datadir = datadir
        self.__header = None
        self.__file = None

    def start_class(self, cls):
        '''Start new class.

        Class and its name are saved in attributes.'''
        self.cls = cls
        self.clsname = cls.cls_name
        self.do_start_class()

    def do_start_class(self):
        '''Start new class self.cls.

        Children are supposed to override this.'''
        pass

    def end_class(self):
        '''Finish the class'''
        self.do_end_class()

    def do_end_class(self):
        '''Finish the class.

        Children are supposed to override this.'''
        pass

    def put_method(self, m):
        '''Generate stuff related to the given public method.

        Children are supposed to override this.'''
        pass

    def start_private(self):
        '''

        First, public methods are put to the output, then this method is called, then private methods are put.'''
        pass

    def put_private_method(self, m):
        '''Generate stuff related to the given private method.

        Children are supposed to override this.'''
        pass

    def put_constructor(self, c):
        '''Generate stuff related to the given constructor.

        Children are supposed to override this.'''
        pass

    def put_struct(self, c):
        '''Generate stuff related to the given struct.

        Structs can be wrapped by a class with setters/getters, for example.

        Children are supposed to override this.'''
        pass

    def do_end(self):
        '''Finish generation of stuff.

        This method is called after end of every class and struct.

        Children are supposed to override this.'''
        pass

    def end(self):
        '''Finish generation of stuff.'''
        self.do_end()
        if self.__file is not None:
            self.__file.close()

    def open_file(self, fname, description='Stuff'):
        '''Start new file with the given name and description.

        First, the (license) header is written, if presented.'''
        self.__file = open(fname, mode='w')
        if self.__header is not None:
            print(self.__header.format(year='2011', name=fname, description=description), file=self.__file)

    def use_header(self, fname):
        '''Set the (license) header to be put to new files.

        It can contain {year}, {name} and {description}, which will be replaced with proper values.'''
        path = os.path.join(self.__datadir, fname)
        f = open(path)
        self.__header = ''.join(f.readlines())

    def put(self, line):
        '''Output new line to the file.'''
        print(line, file=self.__file)

class MultipleGenerator(Generator):
    '''Generate several files for the same class/method at once.

    This class contains list of several other generators, and delegates callbacks to each of them.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.g = []

    def add_generator(self, gen):
        '''Add new generator to the list.'''
        self.g.append(gen)

    def do_start_class(self):
        for gen in self.g:
            gen.start_class(self.cls)

    def do_end_class(self):
        for gen in self.g:
            gen.end_class()

    def put_method(self, m):
        for gen in self.g:
            gen.put_method(m)

    def start_private(self):
        for gen in self.g:
            gen.start_private()

    def put_private_method(self, m):
        for gen in self.g:
            gen.put_private_method(m)

    def put_constructor(self, c):
        for gen in self.g:
            gen.put_constructor(c)

    def put_struct(self, c):
        for gen in self.g:
            gen.put_struct(c)

    def do_end(self):
        for gen in self.g:
            gen.end()

class CxxBaseGen(Generator):
    '''Common stuff for generators of C++ files.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.namer = namer.Cxx()
        self.use_header('license.in')

    @staticmethod
    def make_params(m):
        def stringify_param(p):
            if p.type.real_str() == 'const char *':
                return 'const std::string& ' + p.name
            return str(p)
        return ', '.join(stringify_param(p) for p in m.params)

    @staticmethod
    def get_cxx_type(typ, m):
        if typ.cls_name in model.classes:
            if model.classes[typ.cls_name].result is not None:
                return model.classes[typ.cls_name].result

        t = typ.classy_ret_str()

        if t == 'char *':
            t = 'std::string'
        elif t == 'const char *':
            t = 'std::string'

        if t == 'int':
            if m.func.node.xpath('detaileddescription//simplesect[@kind="par" and title="LIBVLC_RETURN_BOOL"]'):
                t = 'bool'

        return t


class CxxCppGen(CxxBaseGen):
    '''Generate source files (aka implementation) of LibVLC++ classes.'''
    def do_start_class(self):
        self.cname = self.cls.c_name
        self.open_file('vlc++/libvlc_{}.cpp'.format(self.cls.cls_name), '{} implementation'.format(self.cls.cls_name))
        self.put('#include <vlc++/vlc.hpp>\n\nnamespace VLC {\n')
        if not self.cls.copyable:
            self.put('''
{self.clsname}::{self.clsname}({self.cname}* obj) {{
    if (!obj) {{
        throw Exception("Can't construct {self.clsname}");
    }}
    m_obj = obj;
    m_own = true;
}}

{self.clsname}::{self.clsname}({self.clsname}&& another) {{
    m_obj = another.m_obj;
    m_own = another.m_own;
    another.m_obj = NULL;
    another.m_own = false;
}}

const {self.clsname}& {self.clsname}::operator=({self.clsname}&& another) {{
    if (this == &another) {{
        return *this;
    }}
    if (m_own) {{
        {self.cls.releaser}(m_obj);
    }}
    m_obj = another.m_obj;
    m_own = another.m_own;
    another.m_obj = NULL;
    another.m_own = false;
    return *this;
}}

{self.cname}* {self.clsname}::get_c_object() {{
    return m_obj;
}}

const {self.cname}* {self.clsname}::get_c_object() const {{
    return m_obj;
}}

{self.clsname}::~{self.clsname}() {{
    if (m_own) {{
        {self.cls.releaser}(m_obj);
    }}
}}
'''.format(**locals()))
        else:
            self.put('''
{self.clsname}::{self.clsname}({self.cname}* obj) {{
    if (!obj) {{
        throw Exception("Can't construct {self.clsname}");
    }}
    m_obj = obj;
}}

{self.clsname}::{self.clsname}(const {self.clsname}& another) {{
    m_obj = another.m_obj;
    retain();
}}

const {self.clsname}& {self.clsname}::operator=(const {self.clsname}& another) {{
    if (this == &another) {{
        return *this;
    }}
    release();
    m_obj = another.m_obj;
    retain();
    return *this;
}}

bool {self.clsname}::operator==(const {self.clsname}& another) const {{
    return m_obj == another.m_obj;
}}

{self.cname}* {self.clsname}::get_c_object() {{
    return m_obj;
}}

const {self.cname}* {self.clsname}::get_c_object() const {{
    return m_obj;
}}

{self.cname}* {self.clsname}::get_retained_c_object() {{
    retain();
    return m_obj;
}}

{self.clsname}::~{self.clsname}() {{
    release();
}}
'''.format(**locals()))

    @staticmethod
    def make_callparams(f):
        callparams = []
        for p in f.params:
            if p.type.changed:
                callparams.append('{}.get_c_object()'.format(p.name))
            elif p.type.real_str() == 'const char *':
                callparams.append('{}.c_str()'.format(p.name))
            else:
                callparams.append('{}'.format(p.name))
        return ', '.join(callparams)

    @staticmethod
    def get_type_decorators(typ, m):
        t = typ.classy_ret_str()

        if typ.cls_name in model.classes:
            result_maker = model.classes[typ.cls_name].result_maker
            resultfreer = model.classes[typ.cls_name].resultfreer
            if model.classes[typ.cls_name].result is not None:
                t = model.classes[typ.cls_name].result
            return (t, result_maker, resultfreer)

        result_maker = 'c_result'
        resultfreer = ''
        if t == 'char *':
            t = 'std::string'
            resultfreer = '\n    libvlc_free(c_result);'
        elif t == 'const char *':
            t = 'std::string'

        if t == 'int':
            if m.func.node.xpath('detaileddescription//simplesect[@kind="par" and title="LIBVLC_RETURN_BOOL"]'):
                t = 'bool'

        return (t, result_maker, resultfreer)

    def put_private_method(self, m):
        self.put_method(m)

    def __put_overriden(self, m):
        node = m.func.source
        text = node.text
        indent = int(node.attrib.get('indent', '0'))
        if text is not None:
            lines = text.split('\n')
            for l in lines:
                if indent > 0:
                    l = ' ' * indent + l
                else:
                    l = l[-indent:]
                self.put(l.rstrip())

    def put_method(self, m):
        if m.func.overriden:
            self.__put_overriden(m)
            return

        params = self.make_params(m)
        (t, maker, freer) = self.get_type_decorators(m.type, m)

        callparams = self.make_callparams(m)
        if callparams != '':
            callparams = ', ' + callparams

        meth_name = self.namer.make_name(m)

        if t == 'void':
            self.put('''
void {self.clsname}::{meth_name}({params}) {{
    {m.func.name}(m_obj{callparams});
    Exception::checkError();
}}'''.format(**locals()))
        else:
            c_type = m.func.type.real_str()
            self.put('''
{t} {self.clsname}::{meth_name}({params}) {{
    {c_type} c_result = {m.func.name}(m_obj{callparams});
    Exception::checkError();
    {t} result = {maker};{freer}
    return result;
}}'''.format(**locals()))

    def put_constructor(self, c):
        if c.func.overriden:
            self.__put_overriden(c)
            return

        params = self.make_params(c)
        callparams = self.make_callparams(c)

        m_own = ''
        if not self.cls.copyable:
            m_own = '\n    m_own = true;'

        self.put('''
{self.clsname}::{self.clsname}({params}) {{
    m_obj = {c.func.name}({callparams});
    if (!m_obj) {{
        throw Exception("Can't construct {self.clsname}");
    }}{m_own}
}}'''.format(**locals()))

    def do_end_class(self):
        self.put('\n} // namespace VLC\n')

class CxxForwardHppGen(CxxBaseGen):
    '''Generate list of forward declarations of every class.

    Another option would be to track dependencies and do a topological sort of the classes instead.
    '''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.open_file('vlc++/libvlc_common.hpp')
        self.put('''
#ifndef LIBVLC_CXX_COMMON_H
#define LIBVLC_CXX_COMMON_H

#include <stddef.h>
#include <string>
#include <stdexcept>
#include <vlc/vlc.h>

#include <vlc++/libvlc_Exception.hpp>
#include <vlc++/libvlc_structures.hpp>

namespace VLC {
''')

    def do_start_class(self):
        self.put('class {};'.format(self.clsname))

    def do_end(self):
        self.put('\n}\n#endif')

class CxxGlobalHppGen(CxxBaseGen):
    '''Generate LibVLC++ header which includes headers of every class.

    It's main LibVLC++ header to be included by user programs.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.open_file('vlc++/vlc.hpp', 'Main VLC++ header.')
        self.put('''
#ifndef LIBVLC_CXX_VLC_H
#define LIBVLC_CXX_VLC_H
''')

    def do_start_class(self):
        self.put('#include <vlc++/libvlc_{}.hpp>'.format(self.clsname))

    def do_end(self):
        self.put('\n#endif')

class CxxStructsHppGen(CxxBaseGen):
    '''Generate wrappers API for each parsed structure.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.open_file('vlc++/libvlc_structures.hpp', 'LibVLC++ structures')
        self.put('''
#ifndef LIBVLC_CXX_STRUCTURES_H
#define LIBVLC_CXX_STRUCTURES_H
#include <list>
#include <vector>

namespace VLC {
''')

    def put_struct(self, c):
        self.put('''
class {c.cls_name} {{
public:
    /** Not to be used
     */
    {c.cls_name}() {{
        throw std::logic_error("This constructor shouldn't be called.");
    }}
'''.format(**locals()))
        for p in c.params:
            t = {
                'char *'       : 'const std::string&',
                'const char *' : 'const std::string&',
            }.get(p.type, p.type)
            self.put('    {t} {p.name}() const;'.format(**locals()))
        if c.list:
            self.put('\n    static std::list<{c.cls_name}> makeList({c.c_name}* head);'.format(**locals()))
        else:
            self.put('\n    {c.cls_name}({c.c_name}* c);'.format(**locals()))
        self.put('private:')
        if c.list:
            self.put('    {c.cls_name}({c.c_name}* c);'.format(**locals()))
        for p in c.params:
            t = {
                'char *'       : 'std::string',
                'const char *' : 'std::string',
            }.get(p.type, p.type)
            self.put('    {t} m_{p.name};'.format(**locals()))
        self.put('}};\n'.format(**locals()))

    def do_end(self):
        self.put('} // namespace VLC\n#endif')

class CxxStructsCppGen(CxxBaseGen):
    '''Generate implementations for wrappers of each parsed structure.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.open_file('vlc++/libvlc_structures.cpp', 'LibVLC++ structures impl')
        self.put('#include <vlc++/vlc.hpp>\nnamespace VLC {\n')

    def put_struct(self, c):
        for p in c.params:
            t = {
                'char *'       : 'const std::string&',
                'const char *' : 'const std::string&',
            }.get(p.type, p.type)
            self.put('{t} {c.cls_name}::{p.name}() const {{\n    return m_{p.name};\n}}\n'.format(**locals()))
        if c.list:
            self.put('''std::list<{c.cls_name}> {c.cls_name}::makeList({c.c_name}* head) {{
    if (!head) {{
        throw Exception("Can't construct std::list<{c.cls_name}>");
    }}
    std::list<{c.cls_name}> result;
    {c.c_name}* current = head;
    while (current) {{
        result.push_back({c.cls_name}(current));
        current = current->p_next;
    }}
    return result;
}}
'''.format(**locals()))
        self.put('{c.cls_name}::{c.cls_name}({c.c_name}* c) {{'.format(**locals()))
        for p in c.params:
            if p.type in set(('char *', 'const char *')):
                self.put('    m_{p.name} = c->{p.c_name} ? c->{p.c_name} : "";'.format(**locals()))
            else:
                self.put('    m_{p.name} = c->{p.c_name};'.format(**locals()))
        self.put('}}\n'.format())

    def do_end(self):
        self.put('} // namespace VLC')

class CxxIndividualHppGen(CxxBaseGen):
    '''Generate headers with LibVLC++ classes.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.doxy = docgen.Doxygen(self, namer.Cxx())

    def do_start_class(self):
        self.open_file('vlc++/libvlc_{}.hpp'.format(self.clsname), '{} API'.format(self.clsname))
        clsnameup = self.clsname.upper()
        c = self.cls.c_name
        self.put('''
#ifndef LIBVLC_CXX_{clsnameup}_H
#define LIBVLC_CXX_{clsnameup}_H

#include <vlc++/libvlc_common.hpp>

namespace VLC {{
'''.format(**locals()))

        if not self.cls.copyable:
            self.put('''
class {self.clsname} {{
public:
    /**
     * Move underlying {c} object from
     * {self.clsname} another to new {self.clsname}.
     * \\param another existing {self.clsname}
     */
    {self.clsname}({self.clsname}&& another);

    /**
     * Move underlying {c} object from
     * {self.clsname} another to this {self.clsname}.
     * \\param another existing {self.clsname}
     * \\return this
     */
    const {self.clsname}& operator=({self.clsname}&& another);

    ~{self.clsname}();
'''.format(**locals()))
        else:
            self.put('''
class {self.clsname} {{
public:
    /**
     * Copy {c} from another to new {self.clsname} object.
     * \\param another existing {self.clsname}
     */
    {self.clsname}(const {self.clsname}& another);

    /**
     * Copy {c} from another {self.clsname}
     * to this {self.clsname}
     * \\param another existing {self.clsname}
     */
    const {self.clsname}& operator=(const {self.clsname}& another);

    /**
     * Check if 2 {self.clsname} objects contain the same {c}.
     * \\param another another {self.clsname}
     * \\return true if they contain the same {c}
     */
    bool operator==(const {self.clsname}& another) const;

    ~{self.clsname}();
'''.format(**locals()))

        if self.clsname == 'Media':
            self.put('''
    enum ConstructorType {
        Location,
        Path,
        Node,
    };
''')

    def put_constructor(self, c):
        self.put('    // {}'.format(c.func.name))
        self.doxy.put_docs(c, remove_return=True)
        params = self.make_params(c)
        self.put('    {self.clsname}({params});\n'.format(**locals()))

    def put_private_method(self, m):
        self.put_method(m)

    def put_method(self, m):
        t = self.get_cxx_type(m.type, m)
        params = self.make_params(m)
        name = self.namer.make_name(m)
        self.doxy.put_docs(m, skip=1)
        self.put('    {t} {name}({params});\n'.format(**locals()))

    def start_private(self):
        c = self.cls.c_name
        clsname = self.clsname
        self.put('''
    // Compatibility with C API
    /**
     * Construct new {clsname} from {c}*.
     * Note that reference is stolen.
     * \\param obj object
     */
    {clsname}({c}* obj);

    /**
     * Get pointer to underlying {c}.
     * The reference is NOT yours.
     * \\return underlying {c}
     */
    {c}* get_c_object();

    /**
     * Get pointer to underlying {c}.
     * The reference is NOT yours.
     * \\return underlying {c}
     */
    const {c}* get_c_object() const;
'''.format(**locals()))

        if self.cls.copyable:
            self.put('''
    /**
     * Get pointer to underlying {c}.
     * The reference is yours and you'll need to release it.
     * \\return underlying {c}
     */
    {c}* get_retained_c_object();
'''.format(**locals()))

        self.put('private:')

    def do_end_class(self):
        c = self.cls.c_name
        clsname = self.clsname
        if not self.cls.copyable:
            self.put('''
    bool m_own;
    {clsname}& operator=(const {clsname}& another);
    {clsname}(const {clsname}& another);
    bool operator==(const {clsname}& another);'''.format(**locals()))

        self.put('''
    {c}* m_obj;
}};

}} // namespace VLC

#endif
'''.format(**locals()))



class CxxGen(MultipleGenerator):
    '''Generate LibVLC++.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.add_generator(CxxCppGen(*a, **kw))
        self.add_generator(CxxForwardHppGen(*a, **kw))
        self.add_generator(CxxGlobalHppGen(*a, **kw))
        self.add_generator(CxxIndividualHppGen(*a, **kw))
        self.add_generator(CxxStructsHppGen(*a, **kw))
        self.add_generator(CxxStructsCppGen(*a, **kw))

class CxxSWIGGlobalGen(CxxBaseGen):
    '''Generate analog of vlc++/vlc.hpp to be used by SWIG.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.open_file('swig/vlc_all_cxx_headers.i', 'All VLC++ headers together (SWIG version).')
        self.put('''
%include <vlc++/libvlc_common.hpp>
%include <vlc++/libvlc_Exception.hpp>
%include <vlc++/libvlc_structures.hpp>
namespace VLC {
''')

    def do_start_class(self):
        self.put('%include "libvlc_cxx_{}.i"'.format(self.clsname))

    def do_end(self):
        self.put('}')

class CxxSWIGIndividualGen(CxxBaseGen):
    '''Generate SWIG interface files for LibVLC++ classes.'''
    def do_start_class(self):
        self.open_file('swig/libvlc_cxx_{}.i'.format(self.clsname), '{} - SWIG version'.format(self.clsname))
        clsname = self.clsname
        if not self.cls.copyable:
            self.put('''
class {clsname} {{
public:
    ~{clsname}();
'''.format(**locals()))
        else:
            self.put('''
class {clsname} {{
public:
    bool operator==(const {clsname}& another) const;
    ~{clsname}();
'''.format(**locals()))
        if self.clsname == 'Media':
            self.put('''
    enum ConstructorType {
        Location,
        Path,
        Node,
    };
''');


    def put_constructor(self, c):
        params = self.make_params(c)
        self.put('    {self.clsname}({params});'.format(**locals()))

    def put_method(self, m):
        t = self.get_cxx_type(m.type, m)
        params = self.make_params(m)
        name = self.namer.make_name(m)
        self.put('    {t} {name}({params});'.format(**locals()))

    def do_end_class(self):
        c = self.cls.c_name
        clsname = self.clsname
        self.put('''
    {clsname}({c}* obj);
    {c}* get_c_object();
    const {c}* get_c_object() const;
'''.format(**locals()))
        if self.cls.copyable:
            self.put('{c}* get_retained_c_object();'.format(c=c))
        self.put('};')

class CxxSWIGGen(MultipleGenerator):
    '''Generate SWIG interface for LibVLC++.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.add_generator(CxxSWIGIndividualGen(*a, **kw))
        self.add_generator(CxxSWIGGlobalGen(*a, **kw))

class SWIGRenamer(Generator):
    '''Make functions' names to follow naming conventions of the target language.

    Children should define methods: get_output, get_lang, get_namer'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.use_header('license.in')

        fname = self.get_output()
        lang = self.get_lang()
        self.open_file(fname, 'SWIG renames for {}'.format(lang))

        self.namer = self.get_namer()
        self.cxxnamer = namer.Cxx()

    def put_method(self, m):
        newname = self.namer.make_name(m)
        cname = self.cxxnamer.make_name(m)
        cls = self.clsname
        self.put('%rename("{newname}", fullname=1) "VLC::{cls}::{cname}";'.format(**locals()))

class PerlSWIGRenamer(SWIGRenamer):
    '''Make functions' names to follow naming conventions of Perl.'''
    def get_output(self):
        return 'swig/vlc_perl_renames.i'

    def get_lang(self):
        return 'Perl'

    def get_namer(self):
        return namer.Perl()

class PerlPODGen(Generator):
    '''Generate POD documentation for Perl classes.

    Not finished yet. Some stuff is missing.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.use_header('licensep.in')
        self.pod = docgen.POD(self, namer.Perl())

    def do_start_class(self):
        self.open_file('perl/VideoLAN/LibVLC/{}.pod'.format(self.clsname), 'Documentation for {}'.format(self.clsname))
        self.put('...') # FIXME This should include manually written SYNOPSIS.

    def put_constructor(self, c):
        pass # FIXME This should write docs for the constructor.

    def put_method(self, m):
        self.pod.put_docs(m, skip=1)

    def do_end_class(self):
        self.put('...') # FIXME Put some footer

class PerlStructsPODGen(Generator):
    '''Generate POD documentation for wrapped structures.

    Not finished yet.'''
    # FIXME Output something useful here.
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.use_header('licensep.in')
        self.open_file('perl/VideoLAN/LibVLC/structures.pod', 'Structures')

class PerlGen(MultipleGenerator):
    '''Generate Perl bindings.

    SWIG should be run on the output of this.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.add_generator(PerlSWIGRenamer(*a, **kw))
        self.add_generator(PerlPODGen(*a, **kw))
        self.add_generator(PerlStructsPODGen(*a, **kw))

class RubyRDocGen(Generator):
    '''Generate documentation for Ruby classes.

    Not finished yet.'''
    # FIXME Implement also put_constructor, put_struct, do_start_class
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.use_header('license.in')
        self.open_file('swig/vlc_ruby_docs.i', 'Documentation for Ruby bindings')
        self.cxxnamer = namer.Cxx()
        self.rdoc = docgen.RDoc(self, namer.Ruby())

    def put_method(self, m):
        self.put('%feature("autodoc", "')
        self.rdoc.put_docs(m, skip=1)
        self.put('") VLC::{}::{};'.format(self.clsname, self.cxxnamer.make_name(m)))

class RubySWIGRenamer(SWIGRenamer):
    '''Make functions' names to follow naming conventions of Ruby.'''
    def get_output(self):
        return 'swig/vlc_ruby_renames.i'

    def get_lang(self):
        return 'Ruby'

    def get_namer(self):
        return namer.Ruby()

    def put_method(self, m):
        newname = self.namer.make_name(m)
        cname = self.cxxnamer.make_name(m)
        cls = self.clsname
        self.put('%rename("{newname}", fullname=1) "VLC::{cls}::{cname}";'.format(**locals()))
        if newname.endswith('?'):
            newname = newname[0:-1]
            self.put('%alias VLC::{cls}::{cname} "{newname}";'.format(**locals()))

class RubyGen(MultipleGenerator):
    '''Generate Ruby bindings.

    SWIG should be run on the output of this.'''
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.add_generator(RubySWIGRenamer(*a, **kw))
        self.add_generator(RubyRDocGen(*a, **kw))
