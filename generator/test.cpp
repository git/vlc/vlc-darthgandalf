#include <vlc++/vlc.hpp>
#include <iostream>

using std::cout;
using std::endl;
using std::list;

void handle(const list<VLC::TrackDescription>& l) {
    for (list<VLC::TrackDescription>::const_iterator i = l.begin(); i != l.end(); ++i) {
        cout << i->id() << ": [" << i->name() << "]" << endl;
    }
}

void on_play(const struct libvlc_event_t *, void*) {
    cout << "foo" << endl;
}

void on_play2(const struct libvlc_event_t *, void*) {
    cout << "bar" << endl;
}

int main(int argc, char** argv) {
	VLC::Instance inst(0, NULL);
	VLC::Media media(inst, argv[1]);
	VLC::MediaPlayer player = media;

	player.play();
	player.pause();
	sleep(1);

	cout << "Subtitles" << endl;
	handle(player.video().spuDescription());

	cout << "Titles" << endl;
	handle(player.video().titleDescription());

	cout << "Video" << endl;
	handle(player.video().trackDescription());

	cout << "Audio" << endl;
	handle(player.audio().trackDescription());

	player.stop();
}
