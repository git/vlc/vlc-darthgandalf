#############################################################################
# namer.py: Rules for naming methods in different languages
#############################################################################
# Copyright © 2011 the VideoLAN team
#
# Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#############################################################################
'''Rename functions according to naming conventions of different languages.'''

class Namer:
    '''Base class.'''
    def make_name(self, method):
        '''Make a name for the given method.'''
        return self.do_make_name(method.words, method)

    def do_make_name(self, words, method):
        raise NotImplementedError('Need to implement this')

class Cxx(Namer):
    '''Make names like this: fooBarBaz, and without "get".'''
    def do_make_name(self, words, method):
        if words[0] == 'get':
            needed_words = words[1:]
        elif words[-1] == 'get':
            needed_words = words[:-1]
        else:
            needed_words = words
        camel_name = needed_words[0] + ''.join(w[0].upper() + w[1:] for w in needed_words[1:])
        return camel_name

class Perl(Namer):
    '''Make names like this: foo_bar_baz, and without "get"/"set".'''
    def do_make_name(self, words, method):
        if words[0] in set(('get', 'set')):
            words = words[1:]
        elif words[-1] == 'get':
            words = words[:-1]
        return '_'.join(words)

class Ruby(Namer):
    '''Make names like this: foo_bar_baz, without "get", change "set" to "... =" and add "?" for bool getters.'''
    def do_make_name(self, words, method):
        if words[0] in ('get', 'set'):
            needed_words = words[1:]
        elif words[-1] == 'get':
            needed_words = words[:-1]
        else:
            needed_words = words
        if words[0] == 'set':
            suffix = '='
        else:
            suffix = ''
        if method and method.func.node.xpath('detaileddescription//simplesect[@kind="par" and title="LIBVLC_RETURN_BOOL"]'):
            suffix = '?'
        return '_'.join(needed_words) + suffix
