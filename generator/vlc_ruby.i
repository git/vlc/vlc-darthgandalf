/*****************************************************************************
 * vlc_ruby.i: Ruby specifics of LibVLC++ bindings, part 1.
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

%include "vlc_ruby_renames.i"
%include "vlc_ruby_docs.i"

%typemap(in) (int argc, char const*const*argv) {
    int i;
    Check_Type($input, T_ARRAY);
    /* Get the length of the array */
    $1 = RARRAY_LEN($input);
    $2 = (char **) malloc(($1+1)*sizeof(char *));
    /* Get the first element in memory */
    VALUE *ptr = RARRAY_PTR($input);
    for (i = 0; i < $1; i++, ptr++) {
        /* Convert Ruby Object String to char* */
        VALUE tmp = *ptr;
        /* FIXME if StringValueCStr raises an exception, $2 is memleaked. */
        $2[i] = StringValueCStr(tmp);
    }
    $2[i] = NULL; /* End of list */
}

%typemap(freearg) (int argc, char const*const*argv) {
    if ($2) free($2);
}

%typecheck(0) (int argc, char const*const*argv) {
    $1 = TYPE($input) == T_ARRAY;
}

