/*****************************************************************************
 * vlc++/libvlc_Exception.hpp: Exception API
 *****************************************************************************
 * Copyright © 2011 the VideoLAN team
 *
 * Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef LIBVLC_CXX_EXCEPTION_H
#define LIBVLC_CXX_EXCEPTION_H

#include <exception>

namespace VLC {

class Exception : public std::exception {
    std::string message;
public:
    Exception(const std::string& defaultMessage = "Unknown LibVLC error");
    virtual const char* what() const throw();
    ~Exception() throw();

    static void checkError();
};

}

#endif

