#!/usr/bin/env python3
#############################################################################
# run.py: Entry point for LibVLC++ and bindings generator.
#############################################################################
# Copyright © 2011 the VideoLAN team
#
# Authors: Alexey Sokolov <alexey@alexeysokolov.co.cc>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#############################################################################
'''Parse LibVLC headers (via doxygen XML) and output wrappers for different languages.'''

import textwrap
import argparse
import os
import sys

from lxml import etree

import model
import generator

datadir = os.path.dirname(sys.argv[0])

argparser = argparse.ArgumentParser()
argparser.add_argument('xml', action='store', type=argparse.FileType(), help='path to combined doxygen xml output')
argparser.add_argument('--language', '-l', default=['c++'], choices=['c++', 'swig', 'perl', 'ruby'], nargs='+', help='what generator to use. Default is c++')

args = argparser.parse_args()

tree = etree.parse(args.xml)
override = etree.parse(os.path.join(datadir, 'override.xml'))

model.Class(c_name='libvlc_instance_t',          cls_name='Instance',        prefix=''                  ).register()
model.Class(c_name='libvlc_media_player_t',      cls_name='MediaPlayer',     prefix='media_player_'     ).register()
model.Class(c_name='libvlc_media_list_t',        cls_name='MediaList',       prefix='media_list_'       ).register()
model.Class(c_name='libvlc_media_t',             cls_name='Media',           prefix='media_'            ).register()
model.Class(c_name='libvlc_media_list_player_t', cls_name='MediaListPlayer', prefix='media_list_player_').register()
model.Class(c_name='libvlc_media_discoverer_t',  cls_name='MediaDiscoverer', prefix='media_discoverer_',copyable=False,releaser='libvlc_media_discoverer_release').register()
model.Class(c_name='libvlc_media_library_t',     cls_name='MediaLibrary',    prefix='media_library_'    ).register()
#model.Class(c_name='libvlc_log_t',               cls_name='Log',             prefix='log_',            copyable=False, releaser='libvlc_log_close').register()
#model.Class(c_name='libvlc_log_iterator_t',      cls_name='LogIterator',     prefix='log_iterator_',   copyable=False, releaser='libvlc_log_iterator_free').register()
#model.Class(c_name='libvlc_event_manager_t',     cls_name='EventManager',    prefix='event_'            ).register()


model.classes['MediaPlayer'].add_subclass('Audio', 'audio')
model.classes['MediaPlayer'].add_subclass('Video', 'video')

#model.Struct.parse(tree, override, 'libvlc_log_message_t', 'LogMessage').register()
model.Struct.parse(tree, override, 'libvlc_module_description_t', 'ModuleDescription', 'libvlc_module_description_list_release').register()
model.Struct.parse(tree, override, 'libvlc_track_description_t', 'TrackDescription', 'libvlc_track_description_list_release').register()
model.Struct.parse(tree, override, 'libvlc_audio_output_t', 'AudioOutputDescription', 'libvlc_audio_output_list_release').register()
model.Struct.parse(tree, override, 'libvlc_media_track_info_t', 'MediaTrackInfo').register()

skip_functions = set()

for f in override.xpath('/override/function'):
    header = f.xpath('header')[0]
    func = model.wrap_func(header)
    func.overriden = True
    func.source = f.xpath('source')[0]
    if f.attrib.get('override', 'overload') == 'replace':
        skip_functions.add(f.xpath('header/name')[0].text)

for f in tree.xpath('//compounddef[@kind="file"]//memberdef[@kind="function"]'):
    if f.xpath('name')[0].text not in skip_functions:
        model.wrap_func(f)

print('Unwrapped:')
for f in model.unwrapped:
    print(f)

for lang in args.language:
    gen = {
            'c++' : generator.CxxGen,
            'swig': generator.CxxSWIGGen,
            'perl': generator.PerlGen,
            'ruby': generator.RubyGen,
          }[lang](datadir)

    for cls in model.classes.values():
        if not cls.need_wrap:
            continue

        gen.start_class(cls)

        for c in cls.constructors:
            gen.put_constructor(c)

        for m in cls.methods:
            gen.put_method(m)

        gen.start_private()

        for m in cls.private_methods:
            gen.put_private_method(m)

        gen.end_class()

    for cls in model.structs.values():
        gen.put_struct(cls)

    gen.end()
