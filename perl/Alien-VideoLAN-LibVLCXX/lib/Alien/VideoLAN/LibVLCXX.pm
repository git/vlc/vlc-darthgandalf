package Alien::VideoLAN::LibVLCXX;

use warnings;
use strict;
use base 'Alien::VideoLAN::LibVLC';
use File::Spec::Functions qw(catdir);
use Alien::VideoLAN::LibVLCXX::ConfigData;

=head1 NAME

Alien::VideoLAN::LibVLCXX - Finds and installs libvlc++

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Alien::VideoLAN::LibVLCXX;

    my $foo = Alien::VideoLAN::LibVLCXX->new();
    ...

=head1 METHODS

=head2 find_libvlcxx

=cut

sub find_libvlcxx {
	my $self = shift;
	my %a = @_;

	my $buildtype = Alien::VideoLAN::LibVLCXX::ConfigData->config('buildtype');

	if ($buildtype eq 'already_installed') {
		return $self->_find('libvlc++', %a)
	} else {
		my %p;
		my $old_pkg = $ENV{PKG_CONFIG_PATH};
		$ENV{PKG_CONFIG_PATH} = catdir(Alien::VideoLAN::LibVLCXX::ConfigData->config('prefix'), 'lib', 'pkgconfig');
		eval {
			%p = $self->_find('libvlc++', %a);
		};
		$ENV{PKG_CONFIG_PATH} = $old_pkg;
		die $@ if $@;
		return %p;
	}
}

=head2 function2

=cut

sub function2 {
}

=head1 AUTHOR

Alexey Sokolov, C<< <alexey at alexeysokolov.co.cc> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-alien-videolan-libvlcxx at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Alien-VideoLAN-LibVLCXX>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Alien::VideoLAN::LibVLCXX


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Alien-VideoLAN-LibVLCXX>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Alien-VideoLAN-LibVLCXX>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Alien-VideoLAN-LibVLCXX>

=item * Search CPAN

L<http://search.cpan.org/dist/Alien-VideoLAN-LibVLCXX/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Alexey Sokolov.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.


=cut

1; # End of Alien::VideoLAN::LibVLCXX
