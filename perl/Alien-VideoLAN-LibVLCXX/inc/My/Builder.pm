package My::Builder;

use strict;
use warnings;
use base 'Module::Build';

use lib "inc";
use My::Utility qw(get_dlext);
use File::Spec::Functions qw(catdir catfile splitpath catpath rel2abs abs2rel);
use File::Path qw(make_path remove_tree);
use File::Copy qw(cp);
use File::Fetch;
use File::Find;
use Archive::Extract;
use Digest::SHA qw(sha1_hex);
use Text::Patch;
use Config;
use Alien::VideoLAN::LibVLC;
use Alien::CMake;

sub libvlcxx_install_dir {
	my $self = shift;
	return catdir($self->install_destination('arch'), 'auto', 'Alien', 'VideoLAN', 'LibVLCXX', 'x');
}

sub ACTION_build {
	my $self = shift;
	# as we want to wipe 'sharedir' during 'Build clean' we has
	# to recreate 'sharedir' at this point if it does not exist

	my $bp = $self->notes('build_params');
	die "###ERROR### Cannot continue build_params not defined" unless defined($bp);

	printf("Build option used:\n\t%s\n", $bp->{title} || 'n.a.');

	mkdir 'sharedir' unless(-d 'sharedir');
	$self->add_to_cleanup('sharedir');
	$self->SUPER::ACTION_build;
}

sub ACTION_install {
	my $self = shift;
	my $buildtype = $self->notes('build_params')->{buildtype};
	$self->SUPER::ACTION_install;

	my $installdir = $self->libvlcxx_install_dir;

	if (-d $installdir) {
		print "Removing the old $installdir \n";
		remove_tree($installdir);
	}

	if ($buildtype eq 'use_prebuilt_binaries') {
	} elsif ($buildtype eq 'build_from_source') {
		$self->install_binaries
	} elsif ($buildtype eq 'already_installed') {
	}
}

sub ACTION_code {
	my $self = shift;

	my $bp = $self->notes('build_params');
	die "###ERROR### Cannot continue build_params not defined" unless defined($bp);

	# check marker
	if (! $self->check_build_done_marker) {

		# important directories
		my $download     = 'download';
		my $patches      = 'patches';
		# we are deriving the subdir name from $bp->{title} as we want to
		# prevent troubles when user reinstalls the same version of
		# Alien::SDL with different build options
		my $share_subdir = $self->{properties}->{dist_version} . '_' . substr(sha1_hex($bp->{title}), 0, 8);
		my $build_out    = catfile('sharedir', $share_subdir);
		my $build_src    = 'build_src';
		$self->add_to_cleanup($build_src, $build_out);

		# save some data into future Alien::SDL::ConfigData
		$self->config_data('buildtype', $bp->{buildtype});

		if ($bp->{buildtype} eq 'use_prebuilt_binaries') {
			# all the following functions die on error, no need to test ret values
			$self->fetch_binaries($download);
			$self->clean_dir($build_out);
			$self->extract_binaries($download, $build_out);
		} elsif ($bp->{buildtype} eq 'build_from_sources') {
			# all the following functions die on error, no need to test ret values
			$self->fetch_sources($download);
			$self->extract_sources($download, $patches, $build_src);
			$self->clean_dir($build_out);
			$self->build_binaries($build_out, $build_src);
			$self->config_data('prefix', $self->libvlcxx_install_dir); # save it for future Alien::SDL::ConfigData

			$self->config_data('build_params', $bp);
			$self->config_data('build_cc', $Config{cc});
			$self->config_data('build_arch', $Config{archname});
			$self->config_data('build_os', $^O);
		} elsif ($bp->{buildtype} eq 'already_installed') {
		}

		# mark sucessfully finished build
		$self->touch_build_done_marker;
	}

	$self->SUPER::ACTION_code;
}

sub fetch_file {
	my ($self, $url, $sha1sum, $download) = @_;
	die "###ERROR### _fetch_file undefined url\n"     unless @{$url}[0];
	die "###ERROR### _fetch_file undefined sha1sum\n" unless $sha1sum;
	my $ff = File::Fetch->new(uri => @{$url}[0]);
	my $fn = catfile($download, $ff->file);
	if (-e $fn) {
		print "Checking checksum for already existing '$fn'...\n";
		return 1 if $self->check_sha1sum($fn, $sha1sum);
		unlink $fn; #exists but wrong checksum
	}

	my $fullpath;
	foreach my $current_url (@{$url})
	{
		die "###ERROR### _fetch_file undefined url\n" unless $current_url;
		print "Fetching '$current_url'...\n";
		$ff = File::Fetch->new(uri => $current_url);
		$fullpath = $ff->fetch(to => $download);
		last if $fullpath;
	}
	die "###ERROR### Unable to fetch '$ff->file'" unless $fullpath;
	if (-e $fn) {
		print "Checking checksum for '$fn'...\n";
		return 1 if $self->check_sha1sum($fn, $sha1sum);
		die "###ERROR### Checksum failed '$fn'";
	}
	die "###ERROR### _fetch_file failed '$fn'";
}

sub fetch_binaries {
	my ($self, $download) = @_;
	my $bp = $self->notes('build_params');
	$self->fetch_file($bp->{url}, $bp->{sha1sum}, $download);
}

sub fetch_sources {
	my ($self, $download) = @_;
	my $bp = $self->notes('build_params');
	$self->fetch_file($_->{url}, $_->{sha1sum}, $download) foreach (@{$bp->{members}});
}

sub extract_binaries {
	my ($self, $download, $build_out) = @_;

	# do extract binaries
	my $bp = $self->notes('build_params');
	my $archive = catfile($download, File::Fetch->new(uri => @{$bp->{url}}[0])->file);
	print "Extracting $archive...\n";
	my $ae = Archive::Extract->new( archive => $archive );
	die "###ERROR###: Cannot extract $archive ", $ae->error unless $ae->extract(to => $build_out);
}

sub extract_sources {
	my ($self, $download, $patches, $build_src) = @_;
	my $bp = $self->notes('build_params');
	foreach my $pack (@{$bp->{members}}) {
		my $srcdir = catfile($build_src, $pack->{dirname});
		my $unpack = 'y';
		$unpack = $self->prompt("Dir '$srcdir' exists, wanna replace with clean sources?", "y") if (-d $srcdir);
		if (lc($unpack) eq 'y') {
			my $archive = catfile($download, File::Fetch->new(uri => @{$pack->{url}}[0])->file);
			print "Extracting $pack->{pack}...\n";
			my $ae = Archive::Extract->new( archive => $archive );
			die "###ERROR###: cannot extract $pack ", $ae->error unless $ae->extract(to => $build_src);
			foreach my $i (@{$pack->{patches}}) {
				chdir $srcdir;
				my $patch_file = File::Spec->abs2rel( catfile($patches, $i), $srcdir );
				print "Applying patch '$i'\n";
				foreach my $k ($self->patch_get_affected_files($patch_file)) {
					# doing the same like -p1 for 'patch'
					$k =~ s/^[^\/]*\/(.*)$/$1/;
					open(SRC, $k) or die "###ERROR### Cannot open file: '$k'\n";
					my @src = <SRC>;
					close(SRC);
					open(DIFF, $patch_file) or die "###ERROR### Cannot open file: '$patch_file'\n";
					my @diff = <DIFF>;
					close(DIFF);
					foreach(@src)  { $_=~ s/[\r\n]+$//; }
					foreach(@diff) { $_=~ s/[\r\n]+$//; }
					my $out = Text::Patch::patch( join("\n", @src) . "\n", join("\n", @diff) . "\n", { STYLE => "Unified" } );
					open(OUT, ">$k") or die "###ERROR### Cannot open file for writing: '$k'\n";
					print(OUT $out);
					close(OUT);
				}
				chdir $self->base_dir();
			}
		}
	}
	return 1;
}

sub can_build_binaries_from_sources {
	# this needs to be overriden in My::Builder::<platform>
	my $self = shift;
	return 0; # no
}

sub build_binaries {
	# this needs to be overriden in My::Builder::<platform>
	my ($self, $build_out, $build_src) = @_;
	die "###ERROR### My::Builder cannot build SDL from sources, use rather My::Builder::<platform>";
}

sub get_additional_cflags {
	# this needs to be overriden in My::Builder::<platform>
	my $self = shift;
	return '';
}

sub get_additional_libs {
	# this needs to be overriden in My::Builder::<platform>
	my $self = shift;
	return '';
}

sub get_path {
	# this needs to be overriden in My::Builder::<platform>
	my ( $self, $path ) = @_;
	return $path;
}

sub clean_dir {
	my( $self, $dir ) = @_;
	if (-d $dir) {
		remove_tree($dir);
		make_path($dir);
	}
}

sub check_build_done_marker {
	my $self = shift;
	return (-e 'build_done');
}

sub touch_build_done_marker {
	my $self = shift;
	require ExtUtils::Command;
	local @ARGV = ('build_done');
	ExtUtils::Command::touch();
	$self->add_to_cleanup('build_done');
}

sub clean_build_done_marker {
	my $self = shift;
	unlink 'build_done' if (-e 'build_done');
}

sub check_sha1sum {
	my( $self, $file, $sha1sum ) = @_;
	my $sha1 = Digest::SHA->new;
	my $fh;
	open($fh, $file) or die "###ERROR## Cannot check checksum for '$file'\n";
	binmode($fh);
	$sha1->addfile($fh);
	close($fh);
	return ($sha1->hexdigest eq $sha1sum) ? 1 : 0
}

sub patch_get_affected_files {
	my( $self, $patch_file ) = @_;
	open(DAT, $patch_file) or die "###ERROR### Cannot open file: '$patch_file'\n";
	my @affected_files = map{$_ =~ /^---\s*([\S]+)/} <DAT>;
	close(DAT);
	return @affected_files;
}

1;
