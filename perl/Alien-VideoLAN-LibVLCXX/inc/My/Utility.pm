package My::Utility;
use strict;
use warnings;
use base qw(Exporter);

our @EXPORT_OK = qw(check_already_existing check_prebuilt_binaries check_src_build find_file check_header sed_inplace get_dlext);
use Config;
use ExtUtils::CBuilder;
use File::Spec::Functions qw(splitdir catdir splitpath catpath rel2abs);
use File::Find qw(find);
use File::Which;
use File::Copy qw(cp);
use Cwd qw(realpath);

our $cc = $Config{cc};
#### packs with prebuilt binaries
# - all regexps has to match: arch_re ~ $Config{archname}, cc_re ~ $Config{cc}, os_re ~ $^O
# - the order matters, we offer binaries to user in the same order (1st = preffered)
my $prebuilt_binaries = [
    {
      title    => "Binaries Win/32bit SDL-1.2.14 (extended, 20100704) RECOMMENDED\n" .
                  "\t(gfx, image, mixer, net, smpeg, ttf, sound, svg, rtf, Pango)",
      url      => [
        'http://strawberryperl.com/package/kmx/sdl/Win32_SDL-1.2.14-extended-bin_20100704.zip',
        'http://froggs.de/libsdl/Win32_SDL-1.2.14-extended-bin_20100704.zip',
      ],
      sha1sum  => '98409ddeb649024a9cc1ab8ccb2ca7e8fe804fd8',
      arch_re  => qr/^MSWin32-x86-multi-thread$/,
      os_re    => qr/^MSWin32$/,
      cc_re    => qr/gcc/,
    },
    {
      title    => "Binaries Win/32bit SDL-1.2.14 (20090831)\n" .
                  "\t(gfx, image, mixer, net, smpeg, ttf)",
      url      => [
        'http://strawberryperl.com/package/kmx/sdl/lib-SDL-bin_win32_v2.zip',
        'http://froggs.de/libsdl/lib-SDL-bin_win32_v2.zip',
      ],
      sha1sum  => 'eaeeb96b0115462f6736de568de8ec233a2397a5',
      arch_re  => qr/^MSWin32-x86-multi-thread$/,
      os_re    => qr/^MSWin32$/,
      cc_re    => qr/gcc/,
    },
    {
      title    => "Binaries Win/64bit SDL-1.2.14 (extended, 20100824) RECOMMENDED\n" .
                  "\t(gfx, image, mixer, net, smpeg, ttf, sound, svg, rtf, Pango)",
      url      => [
        'http://strawberryperl.com/package/kmx/sdl/Win64_SDL-1.2.14-extended-bin_20100824.zip',	
        'http://froggs.de/libsdl/Win64_SDL-1.2.14-extended-bin_20100824.zip',
      ],
      sha1sum  => 'ccffb7218bcb17544ab00c8a1ae383422fe9586d',
      arch_re  => qr/^MSWin32-x64-multi-thread$/,
      os_re    => qr/^MSWin32$/,
      cc_re    => qr/gcc/,
    },
 ];

#### tarballs with source codes
my $source_packs = [
## the first set for source code build will be a default option
	{
		title => "Download and compile from source",
		prereqs => {
			libs => [
			],
		},
		version => "1.1",
		members => [
			{
				pack => 'vlc++',
				dirname => 'vlc++',
				url => [
					'http://10.156.1.2/~somebody/vlc++-1.1.0-alpha.tar.gz'
				],
				sha1sum => 'ff8916a124a29ca22d17f1b1f685a66cb6aa38f9',
				patches => [
				],
			},
		],
	},
    {
        title => "Download and compile from source",
        prereqs => {},
        version => "1.2",
        members => [
            {
                pack => 'vlc++',
                dirname => 'vlc++',
                url => [
                    'http://10.156.1.2/~somebody/vlc++-1.2.0-alpha.tar.gz'
                ],
                sha1sum => '81aca93b1b0992c7c829dc84b91edfd463a1d31d',
                patches => [],
            }
        ],
    }
];

sub check_already_existing {
	my ($vlc_major, $vlc_minor, $system_wide) = @_;
	my %vlcxx;
	eval {
		%vlcxx = Alien::VideoLAN::LibVLC->_find('libvlc++', suppress_error_message => 1);
	};
	return if $@;
	return if $vlcxx{version} !~ /^$vlc_major\.$vlc_minor\D/;
	my $prefix = ExtUtils::PkgConfig->variable('libvlc++', 'prefix');
	return {
		title     => "Already installed libvlc++ ver=$vlcxx{version} path=$prefix",
		buildtype => 'already_installed',
		syswide   => $system_wide,
	};
}

sub check_prebuilt_binaries
{
  print "Gonna check availability of prebuilt binaries ...\n";
  print "(os=$^O cc=$cc archname=$Config{archname})\n";
  my @good = ();
  foreach my $b (@{$prebuilt_binaries}) {
    if ( ($^O =~ $b->{os_re}) &&
         ($Config{archname} =~ $b->{arch_re}) &&
         ($cc =~ $b->{cc_re}) ) {
      $b->{buildtype} = 'use_prebuilt_binaries';
      push @good, $b;
    }
  }
  #returning ARRAY of HASHREFs (sometimes more than one value)
  return \@good;
}

sub check_src_build {
	my ($vlc_version) = @_; # e.g. "1.1"
	print "Gonna check possibility for building from sources ...\n";
	print "(os=$^O cc=$cc)\n";
	my @good = ();
	foreach my $p (@{$source_packs}) {
		$p->{buildtype} = 'build_from_sources';
		print "CHECKING prereqs for:\n\t$p->{title}\n";
		push @good, $p if $p->{version} eq $vlc_version;
	}
	return \@good;
}

sub check_prereqs_tools {
  my @tools = @_;
  my $ret  = 1;

  foreach my $tool (@tools) {
    
    if((File::Which::which($tool) && -x File::Which::which($tool))
    || ('pkg-config' eq $tool && defined $ENV{PKG_CONFIG} && $ENV{PKG_CONFIG}
                              && File::Which::which($ENV{PKG_CONFIG})
                              && -x File::Which::which($ENV{PKG_CONFIG}))) {
      $ret &= 1;
    }
    else {
      print "WARNING: required '$tool' not found\n";
      $ret = 0;
    }
  }

  return $ret;
}

sub find_file {
  my ($dir, $re) = @_;
  my @files;
  $re ||= qr/.*/;
  {
    #hide warning "Can't opendir(...): Permission denied - fix for http://rt.cpan.org/Public/Bug/Display.html?id=57232
    no warnings;
    find({ wanted => sub { push @files, rel2abs($_) if /$re/ }, follow => 1, no_chdir => 1 , follow_skip => 2}, $dir);
  };
  return @files;
}

sub check_header {
  my ($cflags, @header) = @_;
  print STDERR "Testing header(s): " . join(', ', @header) . "\n";
  my $cb = ExtUtils::CBuilder->new(quiet => 1);
  my ($fs, $src) = File::Temp->tempfile('XXXXaa', SUFFIX => '.c', UNLINK => 1);
  my $inc = '';
  $inc .= "#include <$_>\n" for @header;  
  syswrite($fs, <<MARKER); # write test source code
#if defined(_WIN32) && !defined(__CYGWIN__)
#include <stdio.h>
/* GL/gl.h on Win32 requires windows.h being included before */
#include <windows.h>
#endif
$inc
int demofunc(void) { return 0; }

MARKER
  close($fs);
  my $obj = eval { $cb->compile( source => $src, extra_compiler_flags => $cflags); };
  if($obj) {
    unlink $obj;
    return 1;
  }
  else {
    print STDERR "###TEST FAILED### for: " . join(', ', @header) . "\n";
    return 0;
  }
}

sub sed_inplace {
  # we expect to be called like this:
  # sed_inplace("filename.txt", 's/0x([0-9]*)/n=$1/g');
  my ($file, $re) = @_;
  if (-e $file) {
    cp($file, "$file.bak") or die "###ERROR### cp: $!";
    open INPF, "<", "$file.bak" or die "###ERROR### open<: $!";
    open OUTF, ">", $file or die "###ERROR### open>: $!";
    binmode OUTF; # we do not want Windows newlines
    while (<INPF>) {
     eval( "$re" );
     print OUTF $_;
    }
    close INPF;
    close OUTF;
  }
}

sub get_dlext {
  if($^O =~ /darwin/) { # there can be .dylib's on a mac even if $Config{dlext} is 'bundle'
    return 'so|dylib|bundle';
  }
  elsif( $^O =~ /cygwin/)
  {  
    return 'la';
  }
  else {
    return $Config{dlext};
  }
}

1;
