#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Alien::VideoLAN::LibVLCXX' ) || print "Bail out!
";
}

diag( "Testing Alien::VideoLAN::LibVLCXX $Alien::VideoLAN::LibVLCXX::VERSION, Perl $], $^X" );
