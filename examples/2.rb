#!/usr/bin/env ruby

require 'libvlc'

if ARGV.length == 0
	raise ArgumentError.new('1 arg needed - path to a file')
end

inst = LibVLC::Instance.new
media = LibVLC::Media.new(inst, ARGV[0], :path)
player = LibVLC::MediaPlayer.new(media)

puts 'Playing?', player.is_playing?
player.play

sleep 5
puts 'Volume', player.audio.volume
player.audio.volume = 80
puts player.audio.volume
puts 'Fullscreen?', player.fullscreen?
player.fullscreen = true
puts 'Fullscreen?', player.fullscreen
sleep 5

puts 'Playing?', player.is_playing?
