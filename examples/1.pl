#!/usr/bin/env perl

use strict;
use warnings;

use VideoLAN::LibVLC;

unless (@ARGV) {
	die "1 argument needed - path to a file.";
}

my $inst = VideoLAN::LibVLC::Instance->new;
my $media = VideoLAN::LibVLC::Media->new($inst, path => $ARGV[0]);
my $player = VideoLAN::LibVLC::MediaPlayer->new($media);

$player->play;
sleep 10;
