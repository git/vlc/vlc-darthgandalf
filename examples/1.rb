#!/usr/bin/env ruby

require 'libvlc'

if ARGV.length == 0
	raise ArgumentError.new('1 arg needed - path to a file')
end

inst = LibVLC::Instance.new
media = LibVLC::Media.new(inst, ARGV[0], :path)
player = LibVLC::MediaPlayer.new(media)

player.play
sleep 10
